import numpy as np
import binning 

prefix = '/home/dylan/Blender/NoSlipAnalysis/custom/analysis/noslip_fulldamp/2000.csv'
x_bounds = np.array([[0.5,2.2],
                   [-0.205,0.205],
                   [-0.15,0.15]])
delta = [0.02,0.02,0.02]
dims = [0,1]
D = np.genfromtxt(prefix, delimiter=',',skip_header=1)
    
#x_inds = binning.withinBounds(D[:,1:4],x_bounds)
#data_xremoved = D[x_inds,:]

final_data = D

points = final_data[:,1:4]
vx = final_data[:, 4]
vy = final_data[:, 5]
vz = final_data[:, 6]

xe, ye, avx = binning.binValues(points, delta, dims, vx, x_bounds)
avy         = binning.binValues(points, delta, dims, vy, x_bounds)[2]
avz         = binning.binValues(points, delta, dims, vz, x_bounds)[2]   

Sm  = np.sqrt(avx**2 + avy**2 + avz**2)

binning.binplot2d(xe, ye, Sm, minv=10, interp=True)
#binning.streamPlot2d(xe, ye, avx, avy, Sm, 0.01,1.0)








