import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import stats 

# Masked values will not be used in generating the colour map.
# Masks the values outside the range [minval, maxval]
def maskValues(X, minval, maxval=1000000):
    X_ma = np.ma.array(X)
    X_ma_masked = np.ma.masked_where(X_ma < minval, X_ma)
    return np.ma.masked_where(X_ma_masked > maxval, X_ma_masked)

# Masks the values outside the range [|minval|, |maxval|]. Uses absolute values
def maskAbsolute(X, minval, maxval=1000000):
    X_ma = np.ma.array(X)
    X_ma_masked = np.ma.masked_where(np.absolute(X_ma) < minval, X_ma)
    return np.ma.masked_where(np.absolute(X_ma_masked) > maxval, X_ma_masked)

# Bins discrete points with values assigned.
# Returns the edges of all the bins and the means of the values within those
# bins
def binValues(points, dx_arr, dims, vals, bounds=0):
    x = points[:,dims[0]]
    y = points[:,dims[1]]
    if np.array(bounds).all() == 0:
        x_edges, y_edges = getBinEdges(points, dx_arr, dims)
    else:
        dx = dx_arr[dims[0]]
        dy = dx_arr[dims[1]]
        x_edges = np.arange(bounds[dims[0],0], bounds[dims[0],1]+dx, dx)
        y_edges = np.arange(bounds[dims[1],0], bounds[dims[1],1]+dy, dy)
        
    ret = stats.binned_statistic_2d(x,y,vals,'mean',bins=[x_edges,y_edges])
    return x_edges, y_edges, np.nan_to_num(ret.statistic.T)

# Returns the indices of the points within some bounds
def withinBounds(points, bounds):
    if points.shape[1] != 3:
        return -1
    if bounds.shape != (3,2):
        return -2
    
    valid_x = np.logical_and(points[:,0] >= bounds[0,0],
                                      points[:,0] <= bounds[0,1])
    valid_y = np.logical_and(points[:,1] >= bounds[1,0],
                                      points[:,1] <= bounds[1,1])  
    valid_z = np.logical_and(points[:,2] >= bounds[2,0],
                                      points[:,2] <= bounds[2,1]) 
    
    inds = np.where(np.logical_and(np.logical_and(valid_x, valid_y),valid_z))[0]
    
    return np.array(inds)

# Bins are more commonly defined by the edges. This returns the centres
def getBinCentres(x_edges, y_edges):
    x_centre = (x_edges[1:] + x_edges[:-1])/2.0
    y_centre = (y_edges[1:] + y_edges[:-1])/2.0
               
    return x_centre, y_centre
# Gets edges of bins based on the bin sizes and boundaries
def getBinEdges(points, dx_arr, dims):
    if len(dims) != 2:
        return 0,0
    
    dx = dx_arr[dims[0]]
    dy = dx_arr[dims[1]]
    
    minx = np.amin(points[:,dims[0]])
    maxx = np.amax(points[:,dims[0]])
    miny = np.amin(points[:,dims[1]])
    maxy = np.amax(points[:,dims[1]])
    
    x_edges = np.arange(minx, maxx+dx, dx)
    y_edges = np.arange(miny, maxy+dy, dy)
    
    return x_edges, y_edges
# Transforms from Euclidean to cylindrical coordinates   
def euclidToCylindrical(points_in):
    z = points_in[:,2]
    rho = np.sqrt(points_in[:,0]**2 + points_in[:,1]**2)
    phi = np.arctan2(points_in[:,0], points_in[:,1])
    
    return np.transpose(np.array([rho,phi,z]))
# Bins data and plots a 2D map
def binplot2d(Xe, Ye, S, minv=-1000000, maxv=1000000, interp=False):
    Sm = maskValues(S, minv, maxv)
    cmap = plt.cm.get_cmap('jet', lut=10)
    cmap.set_bad('k')
    fig = plt.figure()
    xlim = Xe[[0,-1]]
    ylim = Ye[[0,-1]]
    ax = fig.add_subplot(111, aspect='equal', xlim=xlim, ylim=ylim)
    
    if(interp):
        Xc, Yc = getBinCentres(Xe, Ye)
        im = mpl.image.NonUniformImage(ax, interpolation='bilinear', cmap=cmap)
        im.set_data(Xc, Yc, Sm)
        ax.images.append(im)
        plt.colorbar(im)
    else:
        X, Y = np.meshgrid(Xe, Ye)
        cmesh = ax.pcolormesh(X, Y, Sm, cmap=cmap)
        plt.colorbar(cmesh)
        
    plt.show()    
# Plots streamlines    
def streamPlot2d(Xe, Ye, U, V, vals, minval=-1000000, maxval=1000000):
    Xc, Yc = getBinCentres(Xe, Ye)
    Um = maskAbsolute(U, minval, maxval)
    Vm = maskAbsolute(V, minval, maxval)
    Sm = maskAbsolute(vals, minval, maxval)
    
    cmap = plt.cm.get_cmap('jet', lut=10)
    cmap.set_bad('k')
    xlim = Xe[[0,-1]]
    ylim = Ye[[0,-1]]
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal', xlim=xlim, ylim=ylim)
    
    ax.streamplot(Xc, Yc, Um, Vm, color='k')
    
    plt.show()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    