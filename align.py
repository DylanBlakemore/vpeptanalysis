import numpy as np
import numpy.linalg as linalg
import itertools
import scipy.misc
  
# load the points from the given files.
#
# it is assumed that each file only contains one tracer, and the locations
# of the tracer is the mean of the locations in the file.  
def load(fnames):
    points = np.zeros((5,3))
    for f in xrange(1,6):
        fname = fnames[f-1]
        points[f-1,:] = np.mean(np.genfromtxt(fname)[:,1:4], axis=0)
    return points

# Fit a circle to the XY points
def fitCircle(x, y):
    #       |x1 y1 1|
    # D =   |       |
    #       |xn yn 1|
    D = np.transpose(np.matrix([x, y, np.ones(x.shape)]))
    #       |-(x1^2 + y1^2)|
    # w =   |              |
    #       |-(xn^2 + yn^2)|
    w = np.transpose(-1 * np.matrix([x**2 + y**2]))
    # listsq solves for a in Da = w such that it minimizes 
    # the 2-norm ||w - Da||
    a = linalg.lstsq(D,w)[0]
    xc = -1* float(a[0]/2)
    yc = -1* float(a[1]/2)
    R = np.sqrt((a[0]**2 + a[1]**2)/4 - a[2])
    return [xc, yc, R, a]    

# Get the normal to a set of points in a plane by finding
# the mean of the total number of cross products between vectors connecting
# points
def getPlaneNormal(p):
    sel_range = range(0,p.shape[0])
    # Find all combinations of 3 points
    C = itertools.combinations(sel_range,3)
    nc = scipy.misc.comb(p.shape[0],3, exact=True)
    normals = np.zeros((nc,3))
    i = 0
    # claculate the cross products of vectors
    for c in C:
        vec1 = p[c[2],:] - p[c[0],:]
        vec2 = p[c[1],:] - p[c[0],:]
        normtmp = np.cross(vec1, vec2)
        normtmp = normtmp / linalg.norm(normtmp)
        normals[i,:] = normtmp[0]
        i = i+1
    
    return np.mean(normals, axis=0)  

# Get a matrix which represents the quaternion rotation from [0, 0, 1] to
# the vector sent as a parameter
def getQuaternionRotation(n):
    zhat = np.array([0,0,1])
    k = -1 * n
    theta = np.arccos(k.dot(zhat))
    b = np.cross(k,zhat)
    bhat = b * 1.0/linalg.norm(b)
    
    q0 = np.cos(theta/2.0)
    q1 = np.sin(theta/2.0)*bhat[0]
    q2 = np.sin(theta/2.0)*bhat[1]
    q3 = np.sin(theta/2.0)*bhat[2]
    
    R = [[q0**2+q1**2-q2**2-q3**2, 2*(q1*q2-q0*q3), 2*(q1*q3+q0*q2)], \
         [2*(q2*q1+q0*q3), q0**2-q1**2+q2**2-q3**2, 2*(q2*q3-q0*q1)], \
         [2*(q3*q1-q0*q2), 2*(q3*q2+q0*q1), q0**2-q1**2-q2**2+q3**2]]
    return np.matrix(R)
    
def getCycloneAlignment(fnames):
    points = load(fnames)
    
    [xc, yc, r, a] = fitCircle(points[:,0], points[:,1])
    cc = np.matrix([xc, yc, np.mean(points[:,2])])
    
    pointsTranslate = np.matrix(points) - np.repeat(cc, points.shape[0], axis=0)
    #circleCentreTranslate = np.mean(pointsTranslate, axis=0)
    n = getPlaneNormal(pointsTranslate)
    R = getQuaternionRotation(n)
    #rotPT = np.transpose(np.matmul(R, np.transpose(pointsTranslate)))
    
    return R, cc

# Transform the data
def transform(data, R=0, cc=0):
    if (data.shape[1] != 3):
        return 0
    
    if cc != 0:
        C = np.repeat(np.matrix(cc), data.shape[0], axis=0)
    else:
        C = 0
        
    if R == 0:
        return np.array(data - C)
    else:
       return np.array(np.transpose(np.matmul(R,np.transpose(data))) - C)
    

