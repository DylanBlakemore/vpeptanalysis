import numpy as np
from scipy import interpolate
from scipy import signal

# Smooths a number of position-time tracks
def sg_filter(data_input, N=5, p=3, d=0):
    # Sort data by time
    data_sorted = data_input[np.argsort(data_input[:,0])]
    track_ids = np.unique(data_sorted[:,4])
    
    filtered = np.zeros(data_sorted.shape)
    i = 0
    
    # iterate through each track
    for tid in track_ids:
        inds = (np.where(data_sorted[:,4] == tid))[0]
        n_i = len(inds)
        
        t = data_sorted[inds,0]
        x = data_sorted[inds,1]
        y = data_sorted[inds,2]
        z = data_sorted[inds,3]
        
        # Divide the time span into equal intervals
        t_int = np.linspace(min(t), max(t), len(t), endpoint=True)
        # Interpolate the positions at the new intervals
        f_x = interpolate.interp1d(t,x)
        f_y = interpolate.interp1d(t,y)
        f_z = interpolate.interp1d(t,z)
        x_int = f_x(t_int)
        y_int = f_y(t_int)
        z_int = f_z(t_int)
        
        # Use a savitzgy-golay filter to smooth the positions
        x_sav = signal.savgol_filter(x_int, N, p, deriv=d)
        y_sav = signal.savgol_filter(y_int, N, p, deriv=d)
        z_sav = signal.savgol_filter(z_int, N, p, deriv=d)
        
        filtered[i:n_i,0] = x_sav
        filtered[i:n_i,1] = y_sav
        filtered[i:n_i,2] = z_sav
        filtered[i:n_i,3] = t_int
        filtered[i:n_i,4] = tid * np.ones(filtered[i:n_i,4].shape)
        
        i = i + n_i
        
    return filtered
