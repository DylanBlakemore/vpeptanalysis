import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import align

def trackplot2d(filename, R=0, c=0):
    return 0

def saveplot3d(data_input, foldername_out, indiv=False, R=0, cc=0):
    data_sorted = data_input[np.argsort(data_input[:,0])]
    track_ids = np.unique(data_sorted[:,4])
    
    if indiv:
        for tid in track_ids:
            outfile = foldername_out + '/trackplot_%03d'%(tid,)  + '.png'
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
        
        
            inds = (np.where(data_sorted[:,4] == tid))[0]
            locations = data_sorted[inds,1:4]
            
            X = align.transform(locations, R, cc)
            
            col = np.random.rand(3,)
            ax.plot(X[:,0],X[:,1],X[:,2],c=col)
           
            plt.xlabel('X')
            plt.ylabel('Y')
            ax.set_aspect('equal','datalim')
            
            fig.savefig(outfile)
            plt.close(fig)
    else:
        outfile = foldername_out + '/trackplot.png'
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        
        for tid in track_ids:
            inds = (np.where(data_sorted[:,4] == tid))[0]
            locations = data_sorted[inds,1:4]
            
            X = align.transform(locations, R, cc)
            
            col = np.random.rand(3,)
            ax.plot(X[:,0],X[:,1],X[:,2],c=col)
           
        plt.xlabel('X')
        plt.ylabel('Y')
        ax.set_aspect('equal','datalim')
        
        fig.savefig(outfile)
        plt.close(fig)

def trackplot3d(data_input, R=0, cc=0):
    data_sorted = data_input[np.argsort(data_input[:,0])]
    track_ids = np.unique(data_sorted[:,4])
        
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    for tid in track_ids:
        inds = (np.where(data_sorted[:,4] == tid))[0]
        locations = data_sorted[inds,1:4]
        
        X = align.transform(locations, R, cc)
        
        col = np.random.rand(3,)
        ax.plot(X[:,0],X[:,1],X[:,2],c=col)
       
    plt.xlabel('X')
    plt.ylabel('Y')
    ax.set_aspect('equal','datalim')
        
    plt.show()    